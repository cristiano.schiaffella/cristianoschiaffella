import React from 'react'
import Head from 'next/head'
import Nav from '../components/nav'
import Header from '../components/header'
import Footer from '../components/footer'
import WindowContainer from '../components/windowContainer'

const Home = () => (
  <div className="homeContainer">
    <Head>
      <title>Cristiano Schiaffella</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <Header/>
    <div className="main">
      <WindowContainer title="Explore" icon="explore.png">404
      <br/>
      TODO:<br/>
      - a dynamic approach to static websites (GatsbyJS) <br/>
      - about regex <br/>
      - ...

      </WindowContainer>
    </div>
    
    <Footer/>

    <style jsx global>{`
      .main {
        max-width: 1020px;
        margin: auto;
        display: flex;
        flex-direction: column;
        height: calc(100vh - 92px);
      }
      body {
        color: white;
        background-color: #18181B;
        min-height: 100vh;
        font-family: Verdana, Geneva, sans-serif;
        margin: 0px;
        padding: 0px;
      }
    `}</style>
  </div>
)

export default Home
