import React, {useEffect, useState} from 'react'
import Head from 'next/head'
import Nav from '../components/nav'
import Header from '../components/header'
import Footer from '../components/footer'
import WindowContainer from '../components/windowContainer'

const Home = () => {

  const [isPlayng, setIsPlayng] = useState(false);

  const loadScript = url => new Promise(resolve => {
    const tag = document.createElement('script');
    tag.async = false;
    tag.src = url;
    const body = document.body;
    body.appendChild(tag);
    tag.addEventListener('load', resolve, {
      once: true
    });
  });
  const urls = { unity: '/unity/Build/UnityLoader.js' };

  /*useEffect(() => {
    Promise.all(Object.values(urls).map(loadScript))
      .then(() => {
        UnityLoader.instantiate("unityContainer", "unity/Build/build.json");
      });
  }, [urls]);*/

  const playGame = () => {
    Promise.all(Object.values(urls).map(loadScript))
    .then(() => {
      UnityLoader.instantiate("unityContainer", "unity/Build/build.json");
      setIsPlayng(true);
    });
  }
 
  return <div className="homeContainer">
    <Head>
      <title>Cristiano Schiaffella</title>
      <link rel="icon" href="/favicon.ico" />
      <script src="/unity/Build/UnityLoader.js"></script>
    </Head>

    <Header/>

    <div className="main">
      <WindowContainer title="Find rooms algorithm" icon="games.png">
      <img src="game_snippet.png" className="gameImage" style={{width: '100%', height: '100%', margin: 'auto'}} onClick={playGame}/>
      <div id="unityContainer" style={{width: '100%', height: '100%', margin: 'auto'}}></div>
      </WindowContainer>
      <div className="gameDescription">
        This is only a demo implementation. Generate a random array and find number of rooms contained in it.
      </div>
      
    </div>
    
    <Footer/>

    <style jsx global>{`
      .main {
        max-width: 960px;
        margin: auto;
        display: flex;
        flex-direction: column;
        padding-bottom: 120px;
      }
      body {
        color: white;
        background-color: #18181B;
        min-height: 100vh;
        font-family: Verdana, Geneva, sans-serif;
        margin: 0px;
        padding: 0px;
      }
      .gameTitle {
        font-family: monospace;
        text-align: center;
        font-size: 16px;
        padding: 40px 0px 20px;
      }
      .gameDescription {
        font-family: monospace;
        text-align: center;
        font-size: 12px;
        padding: 20px 0;
      }
      .gameImage {
        display: ${isPlayng ? 'none' : 'block'};
      }
    `}</style>
  </div>
}

export default Home
