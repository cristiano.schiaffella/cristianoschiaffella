import React from 'react'
import Head from 'next/head'
import Nav from '../components/nav'
import Header from '../components/header'
import Content from '../components/content'
import Footer from '../components/footer'

const Home = () => (
  <div className="homeContainer">
    <Head>
      <title>Cristiano Schiaffella</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <Header/>
    <div className="main">
      <Content/>
    </div>
    
    <Footer/>

    <style jsx global>{`
      .main {
        max-width: 1020px;
        margin: auto;
        display: flex;
        flex-direction: column;
        height: calc(100vh - 92px);
        
      }
      body {
        color: white;
        background-color: #1B1B1B;
        min-height: 100vh;
        font-family: Verdana, Geneva, sans-serif;
        margin: 0px;
        padding: 0px;
        box-shadow: inset 0 0 100px black;
      }
    `}</style>
  </div>
)

export default Home
