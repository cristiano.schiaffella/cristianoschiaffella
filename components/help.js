import React, { useState, useEffect, useRef } from 'react'

const Help = () => {

    const [animationDone, setAnimationDone] = useState(false);

    useEffect(() => {
      setTimeout(() => {
          setAnimationDone(true);
      }, 2500);
  });
    
    //TODO - made clickable helps

    return<div className="descriptions">
        <div className="baloonContainer">
          <div className="baloon">
            Help:<br/>
            <ul>
              <li><b>info</b> get info about myself
                <br/>
                &ensp;options: -v
              </li>
              <li><b>config</b> change os configurations</li>
              <li><b>clear</b> clear the console</li>
              <li><b>contacts</b> show contacts</li>
              <li><b>blog</b> show blog articles
                  <br/>
                  &ensp;usage: blog [article]
                  <br/>
                  &ensp;options: -list
              </li>
            </ul>
          </div>
          <div className="baloonTail">
          </div>
        </div>
        <div>
          <img src="dommy.gif" style={{
            'display': animationDone ? 'none' : 'block'
          }} width="180" />
          <img src="dommy_idle.gif" style={{
            'display': animationDone ? 'block' : 'none'
          }} width="180"/>
        </div>
        <style jsx>{`
            
      .descriptions {
        min-width: 280px;
        max-width: 280px;
        font-family: monospace;
        padding: 20px 0px;
        overflow-y: auto;
        height: calc(100% - 100px);
        margin: 40px 0px;
        display: flex;
        flex-direction: column;
    }
    .baloonContainer {
      flex-grow: 1;
      display: flex;
      flex-direction: column;
    }
    .baloon {
      padding: 20px;
      background-color: white;
      border-radius: 20px;
      color:black;
      flex-grow: 1;
    }
    .baloonTail {
      width: 0; 
      height: 0; 
      border-left: 20px solid transparent;
      border-right: 20px solid transparent;
      border-top: 20px solid white;
      margin-left: 70px;
    }
    .descriptions li {
      margin-bottom: 12px;
      color: grey;
    }
    .descriptions li b{
      cursor: pointer;
      text-decoration: underline;
      color: black;
    }
    @media(max-width:1020px) {
      .descriptions {
        height: 100%;
      margin: 20px auto;
      padding: 0;
      }
      
    }
`}</style>
    </div>
}

export default Help;