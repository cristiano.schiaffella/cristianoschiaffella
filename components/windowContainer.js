import React from 'react';

const WindowContainer = (props) => {
    return <>
    <div className="terminal">
        <ul className="actions">
            <li className="terminalTitleBox"><img src={props.icon}/><div>{props.title}</div></li><li></li><li></li><li></li>
        </ul>
        <div
        className="terminalContent"
        >{props.children}</div>
        <style jsx>{`
        .terminal {
            margin: 4px;
            margin-top: 40px;
            background-color: #f5f2e6;
            height: 100%;
            border-radius: 4px;
            border: 3px solid #c9c09b;
            border-top: 1px solid #c9c09b;
            border-right:  1px solid #c9c09b;
            padding: 4px 4px 4px 4px;
            color: black;
        }
        .terminalTitleBox {
            text-align: left;
            font-family: monospace;
            display: flex !important;
            flex-direction: row;
            padding-top: 4px;
        }
        .terminalTitleBox img {
            max-height: 18px;
            display: block;
            padding-right: 4px;
            transform: translateY(-3px);
        }
        .actions {
            color: black;
            height: 28px;
            width: 100%;
            text-align: right;
            display: flex;
            margin: 0px;
            padding: 0px;
        }
        .actions li:first-child {
            flex-grow: 1;
            background-color: transparent;
            border: 0px;
        }
        .actions li {
            display: block;
            margin: 2px;
            border-radius: 20%;
            height: 20px;
            width: 20px;
            background-color: #c7c0a3;
            border-bottom: 2px solid #aba58a;
            border-left: 2px solid #aba58a;
        }
        .terminalContent {
            border: 1px solid grey;
            height:calc(100% - 38px);
            font-family: monospace;
            max-width: 100%;
        }
        ul.outputs {
            list-style-type: none;
            margin-block-start: unset;
            margin-block-end: unset;
            padding-inline-start: 0px;            
        }
    `}</style>
    </div>
    </>
}

export default WindowContainer;