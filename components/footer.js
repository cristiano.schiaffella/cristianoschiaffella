import React from 'react'

const Footer = () => {
    return <div className="footer">
        <div>Copyright © 2019 - 
        Cristiano Schiaffella <br/> <i> Powered by Next.js</i>
        {false && <><a target="_blank" href="https://www.linkedin.com/in/cristianoschiaffella/"><img src="/linkedin.png"></img></a>
        <a target="_blank" href="https://gitlab.com/cristiano.schiaffella"><img src="/gitlab.png"></img></a></>}

        </div>
<style jsx>{`
        .footer {
            text-align: center;
            color: white;
            height: 30px;
            padding: 4px 0 12px;
            font-family: monospace;
            font-size: 12px;
        }
        .footer img {
            height:18px;
        }
        .footer a {
            float:right;
            margin-left: 8px;
        }
        .footer div {
            max-width: 1020px;
            margin: auto;
        }
        @media(max-width:1020px) {
            .footer i {
                display: none;
            }
        }
    `}</style>
    </div>
}

export default Footer