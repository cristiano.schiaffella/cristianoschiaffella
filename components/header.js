import React, {useState, useEffect} from 'react'
import Link from 'next/link';
import { useRouter } from 'next/router'


const Header = () => {
    const [time, setTime] = useState("");
    const [audio, setAudio] = useState(true);
    const router = useRouter()

    const toggleAudio = () => {
        setAudio(!audio);
    }

    const convertDay = (day) => {
        const days = {
            1: 'Mon',
            2: 'Tue',
            3: 'Wed',
            4: 'Thu',
            5: 'Fri',
            6: 'Sat',
            0: 'Sun'
        }
        return days[day];
    }
    // Set up the interval.
  useEffect(() => {
    tick();

    function tick() {
        let today = new Date();
        let hours = today.getHours().toString();
        hours = hours.length > 1 ? hours : '0'+hours;
        let minutes = today.getMinutes().toString();
        minutes = minutes.length > 1 ? minutes : '0'+minutes;
        let time = convertDay(today.getDay()) + " " + hours + ":" + minutes;
        setTime(time);
    }
    let id = setInterval(tick,1000 * 60);
    return () => clearInterval(id);
  });

  const commands = [
      {
          name: 'Terminal',
          logo: '/terminal.png',
          action: '/'
      },
      {
        name: 'Explore',
        logo: '/explore.png',
        action: '/explore'
      },
      {
        name: 'Games',
        logo: '/games.png',
        action: '/games'
      },
  ];

  const renderCommands = commands.map((elem, key) => (
      <div className="command">
          <Link href={elem.action} >
          <a key={key} onClick={() => {
          }}><img src={elem.logo}></img><div><b><u>{elem.name.charAt(0)}</u></b>{elem.name.substr(1)}</div></a>
          </Link>
          <style jsx>{`
          .command {
              color:black;
              text-transform: none;
              padding:8px 8px 3px;
              cursor: pointer;
              background-color: ${router.pathname === elem.action ? '#c9c09b' : 'inherit'};
          }
          .command:hover {
              background-color: #c9c09b;
          }
          .command::first-letter {
          }
          .command a {
              text-decoration: none;
              color: black;
              display: flex;
          }
          img {
              height: 20px;
              display:block;
              transform: scale(1.2);
          }
          .command div {
              padding-left: 10px;
          }
          
        @media(max-width:1020px) {
            .command div {
                display: none;
            }   
        }
        `}</style>
      </div>
  ));

  //test
  // test2    

    return <div className="header">
    <div className="headerInner">
        <div className="commands">
            {renderCommands}    
        </div>
        <div className="language">EN</div>
        <div className="audio" onClick={toggleAudio}>
    {audio ? <img src="audio_on.png"/> : <img src="audio_off.png"/> }
            </div><div className="infos">{time}</div>
    </div>
    
<style jsx>{`
        .header {
            background-color: #f5f2e6;
            color: #18181B;
            border-bottom: 2px solid #c9c09b;
            font-family: monospace;
            font-size: 16px;
        }
        .headerInner {
            max-width: 1020px;
            margin: auto;
            
            display: flex;
        }
        .audio img {
            padding: 5px 2px 0px;
            max-height:24px;
            cursor: pointer;
        }
        .infos {
            /*background-color: #c9c09b;*/
            padding: 4px 8px;
            border-radius: 2px;
            margin: 4px;
        }
        .language{
            padding: 4px 8px;
            border-radius: 2px;
            margin: 4px;
        }
        .commands {
            flex-grow: 1;
            display: flex;
        }
        
    `}</style>
    </div>
}

export default Header