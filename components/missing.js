import React from 'react'

const Missing = () => (
    <div className={"missing"}>
        In development
        <style jsx>{`
        .missing {
            background-color: #f5f2e6;
            height: 100px;
            width: 200px;
            margin: auto;
            text-align center;
            padding: 40px;
            border-radius: 4px;
            margin-top: 120px;
            border: 3px solid #c9c09b;
            border-top: 1px solid #c9c09b;
            border-right: 1px solid #c9c09b;
            color:black;
        }
`}</style>
    </div>
)

export default Missing;