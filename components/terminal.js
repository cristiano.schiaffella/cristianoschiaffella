import React, { useState, useEffect, useRef } from 'react'
import Newliner from './newliner'
import parseInput from './utils/prompOutput'

const Terminal = () => {

    const [outputs, setOutputs] = useState([]);
    useEffect(() => {
        if (messagesEnd)
            messagesEnd.scrollIntoView()
    });

    let messagesEnd = useRef(null);

    const newInputHandler = (input) => {
        if (input == 'clear') {
            setOutputs([]);
        } else {
            let newOutputs = [...outputs];
            newOutputs.push('C:\\Users\\Cris> '+input+'<br/>' + parseInput(input));
            setOutputs(newOutputs);
        }
        
    }

    return <div className="terminal">
        <ul className="actions">
            <li className="terminalTitleBox"><img src="terminal.png"/><div>Terminal</div></li><li></li><li></li><li></li>
        </ul>
        <div
        className="terminalContent"
        >

        
Cris OS [Version 1.0.0] <br/>
(c) Cristiano Schiaffella 2019. All rights reserved.<br/>
        <ul className="outputs">
            {
                outputs.map((obj, key) => (
                    <li
                    className="output" dangerouslySetInnerHTML={{__html: obj}}>
                    </li>
                ))
            }
        </ul>
            <Newliner onNewInput={newInputHandler}/>
            <div
                ref={(el) => { messagesEnd = el; }}
            >

            </div>
        </div>
        <style jsx>{`
        .terminal {
            background-color: #f5f2e6;
            height: 100%;
            border-radius: 4px;
            border: 3px solid #c9c09b;
            border-top: 1px solid #c9c09b;
            border-right:  1px solid #c9c09b;
            padding: 4px 4px 4px 4px;
        }
        .terminalTitleBox {
            text-align: left;
            font-family: monospace;
            display: flex !important;
            flex-direction: row;
            padding-top: 4px;
        }
        .terminalTitleBox img {
            max-height: 18px;
            display: block;
            padding-right: 4px;
            transform: translateY(-3px);
        }
        .actions {
            color: black;
            height: 28px;
            width: 100%;
            text-align: right;
            display: flex;
            margin: 0px;
            padding: 0px;
        }
        .actions li:first-child {
            flex-grow: 1;
            background-color: transparent;
            border: 0px;
        }
        .actions li {
            display: block;
            margin: 2px;
            border-radius: 20%;
            height: 20px;
            width: 20px;
            background-color: #c7c0a3;
            border-bottom: 2px solid #aba58a;
            border-left: 2px solid #aba58a;
        }
        .terminalContent {
            background-color: black;
            color: #96ED91;
            text-shadow: 2px 0 0 #133614, -2px 0 0 #133614, 0 2px 0 #133614, 0 -2px 0 #133614, 1px 1px #133614, -1px -1px 0 #133614, 1px -1px 0 #133614, -1px 1px 0 #133614;

            border: 1px solid grey;
            height:calc(100% - 38px);
            font-family: monospace;
            padding: 4px;
            overflow-y: scroll;
            max-width: 100%;
        }
        ul.outputs {
            list-style-type: none;
            margin-block-start: unset;
            margin-block-end: unset;
            padding-inline-start: 0px;            
        }
    `}</style>
    </div>
}

export default Terminal;