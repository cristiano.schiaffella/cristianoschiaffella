import React from 'react'
import Terminal from './terminal'
import Help from './help'

const Content = () => {
    return <div className="content">
    <div className="terminalContainer">
        <Terminal />
    </div>
    <Help/>
        
    <style jsx>{`
      .content {
        flex-grow:1;
        display: flex;
        height: 100%;
      }
      .terminalContainer {
          flex-grow: 1;
        padding: 60px;
      }
      @media(max-width:1020px) {
        .terminalContainer {
          max-width: 100%;
          padding: 20px;
          height: 50vh;
        }
        .content {
          flex-direction: column-reverse;
        }
      }
    `}</style>
    </div>
}

export default Content