export default function (input) {
    const regex = /\S*/gm;
    const command = regex.exec(input);
    regex.lastIndex++;
    let option = regex.exec(input);

    option = (option !== null && typeof(option[0]) !== 'undefined') ? option[0] : '';

    switch (command[0].toLowerCase()) {
        case 'info':
            if (option.toLowerCase() == '')
                return `
                ---------------------------------------<br/>
                <b>Cristiano Schiaffella - Software Engineer </b><br/>
                Agile software engineer, I love learn new things, such as frameworks, programming languages, or software development process.<br/>
                I like to unit test my software and spend time designing it, but I also know how to be fast and focused when the challenge is to deliver on time!<br/>
                My main skills as a backend developer are:<br/>
                - PHP 7;<br/>
                - PHP Frameworks: Laravel, Zend Framework 2;<br/>
                - PHPUnit and Codeception for unit testing;<br/>
                - C#;<br/>
                - Regular expression (very passionate!);<br/>
                - MySQL;<br/>
                - MS SQL Sever;<br/>
                - Database architecture;<br/>
                - Design Patterns;<br/>
                <br/>
                As a frontend:<br/>
                - React;<br/>
                - VueJS;<br/>
                - JQuery;<br/>
                - CSS (Sass) & HTML;<br/>
                ---------------------------------------<br/>
                `;
            else if (option.toLocaleLowerCase() == '-v')
                return 'verbose'; //todo
            else 
                return 'Option not found';
        case 'contacts':
            return `
            ---------------------------------------<br/>
            <b>Contacts </b><br/>
            Linkedin: <a  style="text-shadow:none;" href="https://www.linkedin.com/in/cristianoschiaffella/">https://www.linkedin.com/in/cristianoschiaffella/</a></br>
            Gitlab: <a  style="text-shadow:none;" href="https://gitlab.com/cristiano.schiaffella">https://gitlab.com/cristiano.schiaffella</a></br>
            Email: cristiano.schiaffella@gmail.com</br>
            ---------------------------------------<br/>
            `;
        case 'config':
        case 'blog':
            return '<div>Not implemented yet</div>';
        default:
            return '<div>Command not found</div>';
    }
}