import React, {useState, useEffect} from 'react'

const Newliner = (props) => {

    //TODO - use redux

    const [currentInput, setCurrentInput] = useState("");
    const [inputRef, setInputRef] = useState(false);

    useEffect(() => {
        if (inputRef) {
            inputRef.focus();
        }
    });

    const handleKeyPress = (event) => {
        if(event.key === 'Enter'){
          props.onNewInput(currentInput)
          setCurrentInput("");
        }
      }
    const handleChange = (event) => {
        setCurrentInput(event.target.value)
    }

    return <div>
        C:\Users\Cris><input
            type="text"
            onKeyPress={handleKeyPress}
            onChange={handleChange}
            value={currentInput}
            ref={(input) => setInputRef(input)}
        />
        <style jsx>{`
        input[type="text"] {
            background-color: transparent;
            border:0px;
            color: #96ED91;
            text-shadow: 2px 0 0 #133614, -2px 0 0 #133614, 0 2px 0 #133614, 0 -2px 0 #133614, 1px 1px #133614, -1px -1px 0 #133614, 1px -1px 0 #133614, -1px 1px 0 #133614;

            font-family: monospace;
            margin-left: 4px;
            font-size: 12px;
            width: calc(100% - 130px);
        }
        input:focus {
            outline-offset: 0px;
            outline: unset;
        }
        `}</style>
    </div>
}

export default Newliner;